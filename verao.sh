#!/bin/sh

echo "####################################"
echo "# Fazendo backup das configurações #"
echo "####################################"

rm -rf  /usr/share/zoneinfo/Brazil/East.bkp*
cp -av /usr/share/zoneinfo/Brazil/East /usr/share/zoneinfo/Brazil/East.bkp2018
rm -rf /etc/localtime.bkp*
cp -av /etc/localtime  /etc/localtime.bkp2018


echo "####################################"
echo "#   COPIANDO ARQUIVO VERAO.ZIC     #"
echo "####################################"

cp -v /root/verao.zic /usr/share/zoneinfo/Brazil

echo "###########################################"
echo "# SETANDO PARAMETROS NO ARQUIVO VERAO.ZIC #"
echo "###########################################"

chmod -v 644 /usr/share/zoneinfo/Brazil/verao.zic
chown -v root.root /usr/share/zoneinfo/Brazil/verao.zic

echo "##########################"
echo "# EXECUTANDO COMANDO ZIC #"
echo "##########################"

zic -v /usr/share/zoneinfo/Brazil/verao.zic
cp -v /usr/share/zoneinfo/Brazil/East /etc/localtime

/bin/hostname; date